#!/bin/sh

count=`ifconfig | grep 192|wc -l`

if [ $count -ge 1 ]; then
	scp -r build/html/* root@192.168.100.1:/home/doc.titan-engine.net/www/
else
	scp -P2201 -r build/html/* root@210.5.164.14:/home/doc.titan-engine.net/www/
fi
