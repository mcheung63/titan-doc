Architecture
============

Components
----------

The whole project is split into many sub-projects:

 * |titanlink| - It is a software for openstack administrator to operator the openstack cloud. It runs as a standalone java application.
 
 * |titanserverlink| - It is the key component of the whole titan. See below diagram. Cloud administrator uses Titan to connect to Titan-server, send command to it and titan-server will decode the command and redirect to openstack controller. Titan-server got its own database to store its own permission. The DB also stores information for additional functions other than openstack. It is a maven web project.
 
 .. image:: _static/image/titan%20server%20diagram.png
 
 * |titancommonlink| - Is a project storing all common classes used among different projects.
 
 * |titanagentlink| - Is a java program runs inside guest vm, capture status/data of the vm back to titan-server, so the server can do billing function and monitoring function.
 
 * Titan Engine - Is the major component to interpret the titan lang and execute it.
 
 * |titanportallink| - Titan end user portal is for hosting company to provide a single-stop-portal for their own customers. Pretty much like the azure portal or AWS portal. In this portal, it provides functions to admin the components that created in openstack cloud. Such as VM, load balancers, changing network, creating storage and etc...
 
 .. image:: _static/image/titan%20portal.png
 
 * |titanrestserverlink| - It is a j2ee war project, then titan-server startup, it will start an embed jetty to load up this project as its restful api interface. Titan portal send restful api to this project and communicate to the titan-server. See below diagram.

 .. image:: _static/image/titan%20rest%20server%20diagram.png

.. |titanlink| raw:: html

   <a href="https://github.com/mcheung63/titan" target="_blank">Titan</a>
 
.. |titanserverlink| raw:: html

   <a href="https://github.com/mcheung63/titan-server" target="_blank">Titan server</a>
 
.. |titancommonlink| raw:: html

   <a href="https://github.com/mcheung63/titan-common" target="_blank">Titan common library</a>
 
.. |titanagentlink| raw:: html

   <a href="https://github.com/mcheung63/titan-agent" target="_blank">Titan agent</a>
 
.. |titanportallink| raw:: html

   <a href="https://github.com/mcheung63/titan-end-user-portal" target="_blank">Titan portal</a>
 
.. |titanrestserverlink| raw:: html

   <a href="https://github.com/mcheung63/titan-rest-server" target="_blank">Titan rest server</a>
 
 
 
 
 
 
Big picture
-----------

.. image:: _static/image/Titan%20whole%20architecture.png
