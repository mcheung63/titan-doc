Grammers
========

Basic structure
---------------

All code blocks are written between character { and }. Every code block has a name, please make the name uni, when the engine load the language into the execution engine, it doesn't check the name is unique or not. The block name is defined in front of the character { by one line. multiple line is not allowed.

Example:

::

   Block name{
   		// All codes goes here
   }
   
Comment
-------

Two formats:

::

   /*
	comment goes here
   */

And

::

   // comment goes here

Variable
--------

There are two types of variable *primitive* and *predefined*.

Primitive type
~~~~~~~~~~~~~~

At the moment, we only support

* Integer - Stores real number

* Float - For decimal number

* String - a sequence of character


Predefined variables
~~~~~~~~~~~~~~~~~~~~

==============  ======  ==============================================================================================================================================================================================================================================================================================
Variable name   Type    Description
==============  ======  ==============================================================================================================================================================================================================================================================================================
OS_USERNAME     String  In addition to the owning entity (tenant), Nova stores the entity performing the action as the user.
OS_PASSWORD     String  With Keystone you pass the keystone password instead of an api key. Recent versions of novaclient use OS_PASSWORD instead of NOVA_API_KEYs or NOVA_PASSWORD.
OS_TENANT_NAME  String  The introduction of Keystone to the OpenStack ecosystem has standardized the term tenant as the entity that owns resources. In some places references still exist to the original Nova term project for this use. Also, tenant_name is prefered to tenant_id.
OS_AUTH_URL     String  Authenticating against an OpenStack cloud using Keystone returns a Token and Service Catalog. The catalog contains the endpoints for all services the user/tenant has access to - including Nova, Glance, Keystone and Swift.
NO_OF_VM        Int	
VMS             Array	
VMS[0]          Struct	
USERS
==============  ======  ==============================================================================================================================================================================================================================================================================================


Define variable
---------------


Calling Methods
---------------

Looping
-------

If
~~

For
~~~

While
~~~~~

Executions
----------
