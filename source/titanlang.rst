Titan programming language
==========================

Titan lang is a cloud oriented programming language, provide a new way for people to admin/operate cloud.

Why create this language
------------------------

We are facing these problems when using cloud service

 * To start building the cloud, we need to define it. In azure or AWS, all componenets must be setup by hand. We need a way to define the cloud, not only the initialize settings, we also need to define the behavior of different suituation of cloud.
 
 * To handle dynamic suituation such as ddos attack, dynamic scale-up/out of a cloud application. We need logics, static settings cannot accomplish this goal. For example, if you are running a stock trading software oncloud, you forecast the total traffic of your software is depending on the stock market performance of today. Then obvisously we need a program to do analyst the stock market performance and performaning scale-out at the end of the day.  That is why we think Titan lives.
 
 * Script doesn't fit to the suitable I mentioned above. Here are whys:
 
 	- In cloud environment such as openstack, there are more than one components of different services (vm, disk, user rights, etc...). Those component may run in different servers and those servers are separated in different isolated network zone. If you want to use a script to control all of them, you may need to split your script and run it on different machine. It is **time consuming**, **hard to maintain**, **no debug or single step trace support**, **need to deploy scripts to different servers and execute time**

 	- Script is either too simple or too complex. Like the bash script, it is missing many modern programming features such as |oop|. For some big script such as Python, although it supports many moden programming paradigms, but it just too big. If we want to use a programming language to control the cloud, feature like function override, interface are just meaningless.  
 	
 	

.. |oop| raw:: html

   <a href="http://en.wikipedia.org/wiki/Object-oriented_programming" target="_blank">OOP</a>
   
Who will use this language
--------------------------

	Two types of people will use it:
	
	* Cloud administrator - Titan lang bring them additional fesilty to administrate the cloud
	
	* Cloud user - Users can perform
	
		- Defining the cloud in Titan
		
		- Monitoring the cloud using Titan trigger
		
		- Create scale ip/scale out plan using Titan lang
		
		- More...