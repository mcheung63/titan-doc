.. titan-doc documentation master file, created by
   sphinx-quickstart on Sat Jun 14 18:43:23 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Home
====

This is the official Titan document repository.

.. toctree::
   :maxdepth: 4
   :numbered:

   introduction
   architecture
   titanlang
   grammers
   aboutTitan
