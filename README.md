titan-doc
=========

Titan Documents, all documents are made by this project and deploy to http://doc.titan-engine.net

Doc is made by http://sphinx-doc.org

To run this project
-------------------

$make clean html

$./deploy.sh   # this will deploy to http://doc.titan-engine.net
